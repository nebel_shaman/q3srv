FROM ubuntu

RUN apt update
RUN apt install -y wget \
curl \
unzip \
binutils \
bc \
jq \
tmux \
netcat \
lib32gcc1 \
lib32stdc++6 \
python3 \
file \
bsdmainutils \
iproute2

RUN useradd nebel
USER nebel
RUN mkdir /tmp/q3
WORKDIR /tmp/q3

RUN wget -O linuxgsm.sh https://linuxgsm.sh && chmod +x linuxgsm.sh && bash linuxgsm.sh q3server
RUN printf 'Y\nY\nY\n'|./q3server install
RUN cp /tmp/q3/lgsm/config-lgsm/q3server/q3server.cfg /tmp/q3/lgsm/config-lgsm/q3server/q3server_old.cfg
RUN cp /tmp/q3/lgsm/config-lgsm/q3server/_default.cfg /tmp/q3/lgsm/config-lgsm/q3server/q3server.cfg

EXPOSE 27950

CMD ./q3server start
